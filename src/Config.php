<?php

namespace Ohanzee\Config;

use Exception;
use Ohanzee\Helper\Arr;

class Config
{
    /**
     * Configuration readers
     * @var array
     */
    protected $sources = [];

    /**
     * Array of config groups
     * @var array
     */
    protected $groups = [];

    /**
     * Attach a configuration reader. By default, the reader will be added as
     * the first used reader. However, if the reader should be used only when
     * all other readers fail, use `false` for the second parameter.
     *     $config->attach($reader);        // Try first
     *     $config->attach($reader, false); // Try last
     *
     * @param Source  $source instance
     * @param boolean $first  add the reader as the first used object
     *
     * @return  $this
     */
    public function attach(Source $source, $first = true)
    {
        if ($first === true) {
            // Place the log reader at the top of the stack
            array_unshift($this->sources, $source);
        } else {
            // Place the reader at the bottom of the stack
            $this->sources[] = $source;
        }

        // Clear any cached groups
        $this->groups = [];

        return $this;
    }

    /**
     * Detach a configuration reader.
     *     $config->detach($reader);
     *
     * @param Source $source instance
     *
     * @return $this
     */
    public function detach(Source $source)
    {
        if (($key = array_search($source, $this->sources)) !== false) {
            // Remove the writer
            unset($this->sources[$key]);
        }

        return $this;
    }

    /**
     * Load a configuration group. Searches all the config sources, merging all the
     * directives found into a single config group.  Any changes made to the config
     * in this group will be mirrored across all writable sources.
     *     $array = $config->load($name);
     * See [Kohana_Config_Group] for more info
     *
     * @param string $group configuration group name
     *
     * @return Group
     * @throws \Exception
     */
    public function load($group)
    {
        if (!count($this->sources)) {
            throw new Exception('No configuration sources attached');
        }

        if (empty($group)) {
            throw new Exception("Need to specify a config group");
        }

        if (!is_string($group)) {
            throw new Exception("Config group must be a string");
        }

        if (strpos($group, '.') !== false) {
            // Split the config group and path
            list($group, $path) = explode('.', $group, 2);
        }

        if (isset($this->groups[$group])) {
            if (isset($path)) {
                return Arr::path($this->groups[$group], $path, null, '.');
            }

            return $this->groups[$group];
        }

        $config = [];

        // We search from the "lowest" source and work our way up
        $sources = array_reverse($this->sources);

        foreach ($sources as $source) {
            if ($source instanceof Reader) {
                if ($source_config = $source->load($group)) {
                    $config = Arr::merge($config, $source_config);
                }
            }
        }

        $this->groups[$group] = new Group($this, $group, $config);

        if (isset($path)) {
            return Arr::path($config, $path, null, '.');
        }

        return $this->groups[$group];
    }

    /**
     * Copy one configuration group to all of the other writers.
     *     $config->copy($name);
     *
     * @param string $group configuration group name
     *
     * @return $this
     */
    public function copy($group)
    {
        // Load the configuration group
        $config = $this->load($group);

        foreach ($config->as_array() as $key => $value) {
            $this->writeConfig($group, $key, $value);
        }

        return $this;
    }

    /**
     * Callback used by the config group to store changes made to configuration
     *
     * @param string $group Group name
     * @param string $key   Variable name
     * @param mixed  $value The new value
     *
     * @return Config Chainable instance
     */
    public function writeConfig($group, $key, $value)
    {
        foreach ($this->sources as $source) {
            if (!($source instanceof Writer)) {
                continue;
            }

            // Copy each value in the config
            $source->write($group, $key, $value);
        }

        return $this;
    }
}
