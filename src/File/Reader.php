<?php
namespace Ohanzee\Config\File;

use Ohanzee\Config\Reader as BaseReader;

/**
 * File-based configuration reader. Multiple configuration directories can be
 * used by attaching multiple instances of this class to [Kohana_Config].
 * @package        Kohana
 * @category       Configuration
 * @author         Kohana Team
 * @copyright  (c) 2009-2012 Kohana Team
 * @license        http://kohanaframework.org/license
 */
class Reader implements BaseReader
{
    /**
     * The directory where config files are located
     * @var string
     */
    protected $directory = '';

    protected $extension = '.php';

    /**
     * Creates a new file reader using the given directory as a config source
     *
     * @param string $directory Configuration directory to search
     */
    public function __construct($directory = 'config', $extension = null)
    {
        if ($extension !== null) {
            $this->extension = ".{$extension}";
        }
        // Set the configuration directory name
        $this->directory = trim($directory, '/');
    }

    /**
     * Load and merge all of the configuration files in this group.
     *     $config->load($name);
     *
     * @param string $group configuration group name
     *
     * @return $this current object
     */
    public function load($group)
    {
        $path = $this->directory.'/'.$group.$this->extension;

        return include $path;
    }
}
