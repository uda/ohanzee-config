<?php

namespace Ohanzee\Config;

interface Writer extends Source
{
    /**
     * Writes the passed config for $group
     * Returns chainable instance on success or throws
     * Exception on failure
     *
     * @param string $group  The config group
     * @param string $key    The config key to write to
     * @param array  $config The configuration to write
     *
     * @return boolean
     */
    public function write($group, $key, $config);
}
